%include "words.inc"
%define start_dict last_add

extern find_word
extern print_newline
extern string_length
extern read_word
extern exit
extern print_string

global _start

section .data
    error: db "Could not read message or no such key", 0

section .text
_start:
	mov rsi, 255
	sub rsp, 255
	mov rdi, rsp
	call read_word
	test rax,rax
	je .error
	mov rdi,rax
	mov rsi, start_dict
	call find_word
	test rax,rax
	je .error
	add rax, 8
	mov rdi, rax
	mov r9, rax
	call string_length
	add r9, rax
	inc r9
	mov rdi, r9
	mov r8, 1
	jmp .exit
	.error:
		mov r8,2
		mov rdi, error
		jmp .exit
	.exit:
		call print_string
		call print_newline
		call exit
