all: main.asm lib.asm dict.asm
	nasm -f elf64 main.asm
	nasm -f elf64  lib.asm
	nasm -f elf64 dict.asm
	ld -o main dict.o lib.o main.o
clean:
	find . -type f -name '*.o' -delete